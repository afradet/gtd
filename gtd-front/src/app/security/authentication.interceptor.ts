import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Auth} from "aws-amplify";
import {from, Observable} from "rxjs";
import {map, switchMap} from "rxjs/operators";

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(Auth.currentSession())
            .pipe(
                map(s => s.getAccessToken().getJwtToken()),
                map(token => req.clone({headers: req.headers.set(TOKEN_HEADER_KEY, `Bearer ${token}`)})),
                switchMap(authReq => next.handle(authReq))
            );
    }
}

export const authInterceptorProviders = [
    {provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true}
];
