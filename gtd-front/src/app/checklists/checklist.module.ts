import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChecklistRoutingModule } from './checklist-routing.module';
import { ChecklistsListComponent } from './checklists-list/checklists-list.component';
import { ChecklistDetailsComponent } from './checklist-details/checklist-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { ChecklistElementDisplayPipe } from './checklist-details/checklist-element-display.pipe';
import { MatToolbarModule } from '@angular/material/toolbar';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [
    ChecklistsListComponent,
    ChecklistDetailsComponent,
    ChecklistElementDisplayPipe
  ],
    imports: [
        CommonModule,
        ChecklistRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatToolbarModule,
        TranslateModule
    ]
})
export class ChecklistModule { }
