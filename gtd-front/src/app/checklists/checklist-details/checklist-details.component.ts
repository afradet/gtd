import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Checklist } from '../checklist.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ChecklistService } from '../checklist.service';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ChecklistElement } from '../checklist-element.model';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-checklist-details',
  templateUrl: './checklist-details.component.html',
  styleUrls: ['./checklist-details.component.scss']
})
export class ChecklistDetailsComponent implements OnInit {

  @ViewChild('nameInput') nameInput: ElementRef;
  @ViewChild('searchInput') searchInput: ElementRef;
  @ViewChild('newInput') newInput: ElementRef;

  checklist: Checklist;
  checklistElementForm: UntypedFormGroup;
  checklistNameForm: UntypedFormGroup;
  hasChange = false;
  editMode = false;
  filtered = false;
  showSearch = false;
  search = '';
  showAll = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ChecklistService,
    private fb: UntypedFormBuilder,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.checklistElementForm = this.newElementForm();
    this.route.params.subscribe(params => {
      if (params.id != 'new') {
        this.service.get(Number(params.id)).subscribe(result => {
          this.checklist = result;
          this.checklistNameForm = this.newNameForm();
        });
      } else {
        this.checklist = new Checklist();
        this.editMode = true;
        this.checklistNameForm = this.newNameForm();
      }
    });
  }

  saveElement() {
    if (!this.checklistElementForm.valid) {
      return;
    }
    const cke: ChecklistElement = this.checklistElementForm.value;
    if (!!cke.id) {
      this.checklist.checklistElements = this.checklist.checklistElements
      .map(element => element.id == cke.id ? cke : element);
    } else {
      this.checklist.checklistElements.push(cke);
    }
    this.checklistElementForm.reset();
    this.hasChange = true;
  }

  cancel() {
    this.checklistElementForm.reset();
  }

  editElement(idx: number) {
    this.checklistElementForm.patchValue(this.checklist.checklistElements[idx]);
    this.setElementInputFocus();
  }

  deleteElement(idx: number) {
    this.checklist.checklistElements.splice(idx, 1);
    this.hasChange = true;
  }

  toggleFilter() {
    this.showAll = !this.showAll
  }

  toggleNameEdit() {
    this.editMode = !this.editMode;
    this.setNameInputFocus();
  }

  toggleChecked(cke: ChecklistElement) {
    cke.checked = !cke.checked;
    this.hasChange = true;
  }

  editTitle() {
    this.checklist.name = this.checklistNameForm.value.name;
    this.checklistNameForm = this.newNameForm();
    this.editMode = false;
    this.hasChange = true;
  }

  toggleSort() {
    if (this.filtered) {
      this.checklist.checklistElements = this.checklist.checklistElements
        .sort((cke1, cke2) => cke1.label > cke2.label ? -1 : 1)
    } else {
      this.checklist.checklistElements = this.checklist.checklistElements
        .sort((cke1, cke2) => cke1.label > cke2.label ? 1 : -1)
    }
    this.filtered = !this.filtered;
  }

  toggleSearch() {
    this.showSearch = !this.showSearch;
    if (this.showSearch) {
      this.searchInput.nativeElement.focus();
    }
  }

  resetList() {
    this.service.reset(this.checklist.id).subscribe(result => {
      this.checklist = result;
      this.checklistNameForm = this.newNameForm();
    });
  }

  save() {
    this.saveElement();
    this.service.save(this.checklist).pipe(
      tap(result => {
        this.checklist = result;
        this.toastr.success('Checklist - Save');
        this.checklistNameForm = this.newNameForm();
        this.editMode = false;
        this.hasChange = false;
      }))
      .subscribe();
  }

  private setNameInputFocus() {
    setTimeout(() => {
      if (this.nameInput == undefined) {
        this.setNameInputFocus();
      } else {
        this.nameInput.nativeElement.focus();
      }
    }, 20);
  }

  private setElementInputFocus() {
    this.newInput.nativeElement.focus();
  }

  private newElementForm() {
    return this.fb.group({
      id: [null],
      label: ['', Validators.required],
      checked: [false]
    });
  }

  private newNameForm() {
    return this.fb.group({
      name: [this.checklist.name, Validators.required]
    })
  }
}
