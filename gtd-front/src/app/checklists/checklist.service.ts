import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Checklist } from './checklist.model';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

  private readonly BASE_URL: string = environment.baseUrl + "/rest/checklists";

  constructor(private httpClient: HttpClient) {
  }

  get(idChecklist: number): Observable<Checklist> {
    return this.httpClient.get<Checklist>(`${this.BASE_URL}/${idChecklist}`);
  }

  save(checklist: Checklist): Observable<Checklist> {
    if (!!checklist.id) {
      return this.httpClient.put<Checklist>(`${this.BASE_URL}/${checklist.id}`, checklist)
    }
    return this.httpClient.post<Checklist>(this.BASE_URL, checklist);
  }

  delete(idChecklist: number) {
    return this.httpClient.delete(`${this.BASE_URL}/${idChecklist}`);
  }

  getAll(): Observable<Checklist[]> {
    return this.httpClient.get<Checklist[]>(this.BASE_URL);
  }

  reset(idChecklist: number): Observable<Checklist> {
    return this.httpClient.post<Checklist>(`${this.BASE_URL}/${idChecklist}?action=RESET`, null);
  }
}
