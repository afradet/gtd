import { Component, OnInit } from '@angular/core';
import { Checklist } from '../checklist.model';
import { ChecklistService } from '../checklist.service';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-checklists-list',
  templateUrl: './checklists-list.component.html',
  styleUrls: ['./checklists-list.component.scss']
})
export class ChecklistsListComponent implements OnInit {
  checklists: Checklist[];

  constructor(
    private checklistService: ChecklistService,
    private toastr: ToastrService,
  ) {
  }

  ngOnInit() {
    this.loadChecklist().subscribe();
  }

  deleteChecklist(checklist: Checklist) {
    const id = checklist.id;
    this.checklistService.delete(id).pipe(
        tap(()=> this.checklists = this.checklists.filter(c => c.id != id)),
        tap(()=> this.toastr.success('Checklist - Delete')),
    ).subscribe();
  }

  loadChecklist(): Observable<Checklist[]> {
    return this.checklistService.getAll().pipe(
      tap(results => this.checklists = results)
    );
  }
}
