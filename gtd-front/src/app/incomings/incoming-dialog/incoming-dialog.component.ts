import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Incoming } from '../incoming.model';

@Component({
  selector: 'gtd-incoming-dialog',
  templateUrl: './incoming-dialog.component.html',
  styleUrls: ['./incoming-dialog.component.scss']
})
export class IncomingDialogComponent {

  @ViewChild('incomingTitle') incomingTitle: ElementRef<HTMLElement>;

  incomingForm: UntypedFormGroup;
  title: string;
  addAnother = false;
  incomingsToSave: Incoming[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Incoming,
    private dialogRef: MatDialogRef<IncomingDialogComponent>,
    private fb: UntypedFormBuilder
  ) {
    this.initForm();
    dialogRef.backdropClick().subscribe(()=> {
      this.dialogRef.close(this.incomingsToSave);
    });
  }

  initForm() {
    this.setEmptyForm();
    this.incomingForm.patchValue(this.data);
    this.setIncomingTitleFocus();
  }

  onSubmit() {
    this.incomingsToSave.push(this.incomingForm.value)
    if (this.addAnother) {
      this.setEmptyForm();
      this.setIncomingTitleFocus();
      this.addAnother = false;
    } else {
      this.dialogRef.close(this.incomingsToSave)
    }
  }

  onCancel() {
    this.dialogRef.close(this.incomingsToSave);
  }

  private setIncomingTitleFocus() {
    setTimeout(() => {
      if (this.incomingTitle == undefined) {
        this.setIncomingTitleFocus();
      } else {
        this.incomingTitle.nativeElement.focus();
      }
    }, 20);
  }

  private setEmptyForm() {
    this.incomingForm = this.fb.group({
      id: [''],
      title: ['', Validators.required],
      content: [''],
      creationDate: [''],
      completionDate: [''],
      completed: [false],
    });
  }
}
