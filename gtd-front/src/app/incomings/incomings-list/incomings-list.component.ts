import {Component, OnDestroy, OnInit} from '@angular/core';
import {Incoming} from '../incoming.model';
import {ToastrService} from 'ngx-toastr';
import {Select, Store} from '@ngxs/store';
import {IncomingStore} from '../../shared/store/incoming-store.actions';
import {IncomingStoreState} from '../../shared/store/incoming-store.state';
import {Observable, of, Subscription} from 'rxjs';
import {filter, switchMap, tap} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {IncomingDialogComponent} from '../incoming-dialog/incoming-dialog.component';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'gtd-incomings-list',
  templateUrl: './incomings-list.component.html',
  styleUrls: ['./incomings-list.component.scss']
})
export class IncomingsListComponent implements OnInit, OnDestroy {

  @Select(IncomingStoreState.getIncomings) incomings$: Observable<Incoming[]>
  @Select(IncomingStoreState.selectedIncoming) incoming$: Observable<Incoming>

  showForm = false;
  private subs: Subscription[] = [];

  constructor(
    private dialog: MatDialog,
    private toastr: ToastrService,
    private store: Store,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    if (!!this.route.snapshot.queryParamMap.get('formOpen')) {
      this.showForm = true;
    }
    this.subs.push(
      this.incoming$.pipe(
        filter(() => this.showForm),
        switchMap(incoming => this.dialog.open<IncomingDialogComponent, Incoming, Incoming[]>(IncomingDialogComponent, { data: incoming }).afterClosed()),
        switchMap(incomings => incomings?.length > 0 ? this.store.dispatch(new IncomingStore.SaveAll(incomings)) : of())
      ).subscribe(),
      this.store.dispatch(IncomingStore.LoadAll).subscribe())
  }

  deleteIncoming(id: number) {
    this.store.dispatch(new IncomingStore.Delete(id)).pipe(
      tap(() => this.toastr.success('Incoming - Delete'))
    ).subscribe();
  }

  create() {
    this.showForm = true
    return this.store.dispatch(new IncomingStore.Select(null)).subscribe()
  }

  update(id: number) {
    this.showForm = true
    return this.store.dispatch(new IncomingStore.Select(id)).subscribe()
  }

  trackBy(index, incoming: Incoming) {
    return incoming.id;
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
