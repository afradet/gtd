import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncomingCardComponent } from './incoming-card/incoming-card.component';
import { IncomingOrganizerComponent } from './incoming-organizer/incoming-organizer.component';
import { IncomingsListComponent } from './incomings-list/incomings-list.component';
import { IncomingRoutingModule } from './incoming-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NextActionFormModule } from '../shared/next-action-form/next-action-form.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { SharedPipesModule } from '../shared/pipes/shared-pipes.module';
import { MatDialogModule } from '@angular/material/dialog';
import { IncomingDialogComponent } from './incoming-dialog/incoming-dialog.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import {MatIconModule} from "@angular/material/icon";
import {ClipboardModule} from "@angular/cdk/clipboard";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    IncomingsListComponent,
    IncomingCardComponent,
    IncomingOrganizerComponent,
    IncomingDialogComponent,
  ],
    imports: [
        CommonModule,
        IncomingRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NextActionFormModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        SharedPipesModule,
        MatDialogModule,
        TextFieldModule,
        MatIconModule,
        ClipboardModule,
        TranslateModule
    ]
})
export class IncomingModule {
}
