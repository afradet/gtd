import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingOrganizerComponent } from './incoming-organizer.component';

describe('IncomingOrganizerComponent', () => {
  let component: IncomingOrganizerComponent;
  let fixture: ComponentFixture<IncomingOrganizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomingOrganizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingOrganizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
