import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { IncomingsListComponent } from './incomings-list/incomings-list.component';
import { IncomingOrganizerComponent } from './incoming-organizer/incoming-organizer.component';

const routes: Routes = [
  { path: '', component: IncomingsListComponent },
  { path: 'organizer', component: IncomingOrganizerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncomingRoutingModule {
}
