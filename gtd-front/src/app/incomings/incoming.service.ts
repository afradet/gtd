import { Injectable } from '@angular/core';
import { Incoming } from './incoming.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IncomingService {

  /**
   * TODO :
   * - error management
   * - return on post / update
   */

  private readonly BASE_URL: string = environment.baseUrl + "/rest/incomings";

  constructor(private httpClient: HttpClient) { }

  save(incoming: Incoming): Observable<Incoming> {
    if (incoming.id) {
      return this.httpClient.put<Incoming>(`${this.BASE_URL}/${incoming.id}`, incoming);
    }
    return this.httpClient.post<Incoming>(this.BASE_URL, incoming);
  }

  delete(id:number) {
    return this.httpClient.delete(`${this.BASE_URL}/${id}`);
  }


  getAll(): Observable<Incoming[]> {
    return this.httpClient.get<Incoming[]>(this.BASE_URL);
  }
}
