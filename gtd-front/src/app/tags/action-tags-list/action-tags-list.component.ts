import {Component, OnInit} from '@angular/core';
import {ActionTagService} from '../../shared/action-tag.service';
import {ActionTag} from '../../shared/action-tag.model';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {catchError, tap} from 'rxjs/operators';
import {Select, Store} from "@ngxs/store";
import {TagStoreState} from "../../shared/store/tag-store.state";
import {Observable} from "rxjs";
import {Tag} from "../../shared/store/tag.actions";

@Component({
  selector: 'app-action-tags-list',
  templateUrl: './action-tags-list.component.html',
  styleUrls: ['./action-tags-list.component.scss']
})
export class ActionTagsListComponent implements OnInit {

  @Select(TagStoreState.getTags) tags$: Observable<ActionTag[]>
  tags: ActionTag[];
  tagForm: UntypedFormGroup;

  constructor(
      private store: Store,
      private toastr: ToastrService,
      private fb: UntypedFormBuilder) {
  }

  ngOnInit() {
    this.tagForm = this.generateTagForm();
    this.tags$.pipe(
        tap(tags => this.tags = tags)
    ).subscribe();
  }

  generateTagForm(id: number = null) {
    const tag = this.tags?.find(tag => tag.id == id);
    return this.fb.group({
      id: [id],
      label: [tag?.label, Validators.required],
      color: [tag?.color, Validators.required],
      icon: [tag?.icon, Validators.required]
    });
  }

  editTag(id: number) {
    this.tagForm = this.generateTagForm(id);
  }

  submitForm() {
    const tag: ActionTag = this.tagForm.value;
    this.store.dispatch(new Tag.Save(tag))
    .pipe(
        tap(() => this.toastr.success('ActionTag - Save')),
        tap(() => this.tagForm = this.generateTagForm()),
    ).subscribe();
  }

  get color(): string {
    return this.tagForm?.controls['color'].value;
  }

  set color(value: string) {
    this.tagForm?.controls['color'].setValue(value);
  }
}
