import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionTagsListComponent } from './action-tags-list.component';

describe('ActionTagsListComponent', () => {
  let component: ActionTagsListComponent;
  let fixture: ComponentFixture<ActionTagsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionTagsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionTagsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
