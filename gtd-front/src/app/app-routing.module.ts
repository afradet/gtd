import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    { path: '',   redirectTo: '/incomings', pathMatch: 'full' },
    {
        path: 'incomings',
        loadChildren: () => import('./incomings/incoming.module').then(m => m.IncomingModule)
    },
    {
        path: 'checklists',
        loadChildren: () => import('./checklists/checklist.module').then(m => m.ChecklistModule)
    },
    {
        path: 'nextActions',
        loadChildren: () => import('./next-actions/next-actions.module').then(m => m.NextActionsModule)
    },
    {
        path: 'projects',
        loadChildren: () => import('./projects/projects.module').then(m => m.ProjectsModule)
    },
    {
        path: 'tags',
        loadChildren: () => import('./tags/tags.module').then(m => m.TagsModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
