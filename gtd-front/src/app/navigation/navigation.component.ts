import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'gtd-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Output('logout')
  logoutEvent: EventEmitter<void> = new EventEmitter<void>();

  isHandset = false;
  isSidenavOpen = false;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.breakpointObserver.observe(Breakpoints.Handset).pipe(
      tap(result => this.isHandset = result.matches)
    ).subscribe();

    // close sidenar on route change
    this.router.events.subscribe(
      () => this.isSidenavOpen = false
    )
  }

  logout() {
    this.logoutEvent.emit();
  }
}
