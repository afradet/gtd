import { TestBed } from '@angular/core/testing';

import { NextActionService } from './next-action.service';

describe('NextActionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NextActionService = TestBed.get(NextActionService);
    expect(service).toBeTruthy();
  });
});
