import {ActionTag} from './action-tag.model';
import {ProjectLight} from "./project-light.model";

export class NextAction {

  id: number;
  label: string;
  content: string;
  completed: boolean;
  completionDate: Date;
  creationDate: Date;
  deadlineDate: Date;
  actionPosition: number;
  actionType: ActionType;
  project: ProjectLight;
  actionTags: ActionTag[];

  public constructor(init?: Partial<NextAction>) {
    Object.assign(this, init);
  }
}

export enum ActionType {
  STANDARD = "STANDARD",
  WAITING = "WAITING",
  MAYBE = "MAYBE"
}
