import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActionType, NextAction } from './next-action.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NextActionService {

  private readonly BASE_URL: string = environment.baseUrl + "/rest/nextActions";

  constructor(private httpClient: HttpClient) { }

  get(id: number): Observable<NextAction> {
    return this.httpClient.get<NextAction>(`${this.BASE_URL}/${id}`);
  }

  save(nextAction: NextAction): Observable<NextAction> {
    return this.httpClient.post<NextAction>(this.BASE_URL, nextAction);
  }

  update(nextAction: NextAction): Observable<NextAction> {
    return this.httpClient.put<NextAction>(`${this.BASE_URL}/${nextAction.id}`, nextAction);
  }

  delete(id:number): Observable<any> {
    return this.httpClient.delete(`${this.BASE_URL}/${id}`);
  }

  getAll(type?: ActionType, tagId?: number): Observable<NextAction[]> {
    let params: HttpParams = new HttpParams();
    if (type) params = params.set('actionType', type);
    if (tagId) params =  params.set('tagId', tagId);
    return this.httpClient.get<NextAction[]>(this.BASE_URL, { 'params': params });
  }

  getExport(): Observable<Blob> {
    return this.httpClient.get(`${this.BASE_URL}/export`, { responseType: 'blob'})
  }
}
