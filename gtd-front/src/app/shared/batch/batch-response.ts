import { BatchResult } from './batch-result';

export interface BatchResponse<T> {
  results: BatchResult<T>[];
}
