import { BatchOperation } from './batch-operation';

export class BatchRequest<T> {

  operations: BatchOperation<T>[];

  public constructor(init?: Partial<BatchRequest<T>>) {
    Object.assign(this, init);
  }
}
