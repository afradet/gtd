import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class TextShortenerPipe implements PipeTransform {

  transform(text: string, length = 150): string {
    if (text.length > length) {
      return text.substring(0, length - 3) + '...';
    }
    return text;
  }

}
