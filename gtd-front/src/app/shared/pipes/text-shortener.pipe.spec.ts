import { TextShortenerPipe } from './text-shortener.pipe';

describe('TextShortenerPipe', () => {
  it('create an instance', () => {
    const pipe = new TextShortenerPipe();
    expect(pipe).toBeTruthy();
  });

  it('works', () => {
    const pipe = new TextShortenerPipe();
    expect(pipe.transform('a'.repeat(10))).toEqual('a'.repeat(10));
    expect(pipe.transform('a'.repeat(200))).toEqual('a'.repeat(147) + '...');
    expect(pipe.transform('a'.repeat(200), 25)).toEqual('a'.repeat(22) + '...');
  });
});
