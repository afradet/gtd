import { Incoming } from '../../incomings/incoming.model';

export namespace IncomingStore {

  export class Select {
    public static readonly type = "[IncomingStore] Select an incoming with id";
    constructor(public incomingId: number) { }
  }

  export class SelectNext {
    public static readonly type = "[IncomingStore] Select next incoming";
    constructor() { }
  }

  export class LoadAll {
    public static readonly type = "[IncomingStore] Load incomings";
    constructor() { }
  }

  export class Delete {
    public static readonly type = "[IncomingStore] Delete incoming";
    constructor(public incomingId: number) { }
  }

  export class Save {
    public static readonly type = "[IncomingStore] Create incoming";
    constructor(public incoming: Incoming) { }
  }

  export class SaveAll {
    public static readonly type = "[IncomingStore] Create multiple incomings";
    constructor(public incomings: Incoming[]) { }
  }
}
