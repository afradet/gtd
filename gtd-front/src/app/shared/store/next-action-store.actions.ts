import { NextActionFilter } from './next-action-store.state';
import { NextAction } from '../next-action.model';

export namespace NextActionStore {

  export class LoadAll {
    public static readonly type = "[NextActionStore] Load Actions";

    constructor(public force = false) {
    }
  }

  export class Filter {
    public static readonly type = "[NextActionStore] Filter Actions";

    constructor(public filter: NextActionFilter) {
    }
  }

  export class FinishAction {
    public static readonly type = "[NextActionStore] Finish Action";

    constructor(public actionId: number) {
    }
  }

  export class FinishActionAndProject {
    public static readonly type = "[NextActionStore] Finish Action and Project";

    constructor(public actionId: number) {
    }
  }

  export class FinishActionAndAddNew {
    public static readonly type = "[NextActionStore] Finish Action and Add New";

    constructor(public actionId: number, public newAction: NextAction) {
    }
  }

  export class UpdateAction {
    public static readonly type = "[NextActionStore] Update an action";

    constructor(public action: NextAction) {
    }
  }

  export class Select {
    public static readonly type = "[NextActionStore] Select an Action";

    constructor(public actionId: number) {
    }
  }

  export class CreateAction {
    public static readonly type = "[NextActionStore] Create Action";

    constructor(public newAction: NextAction) {
    }
  }
}
