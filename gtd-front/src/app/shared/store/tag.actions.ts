import {ActionTag} from "../action-tag.model";

export namespace Tag {

  export class FetchAll {
    static readonly type = '[Tag] Fetch All';
  }

  export class Save {
    static readonly type = '[Tag] Save';

    constructor(public tag: ActionTag) {
    }
  }
}
