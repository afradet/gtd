import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextActionDetailsComponent } from './next-action-details.component';

describe('NextActionDetailsComponent', () => {
  let component: NextActionDetailsComponent;
  let fixture: ComponentFixture<NextActionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextActionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextActionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
