import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NextAction} from '../../shared/next-action.model';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {ActionTag} from '../../shared/action-tag.model';
import {Observable, Subscription} from 'rxjs';
import {map, switchMap, tap} from 'rxjs/operators';
import {Select, Store} from '@ngxs/store';
import {NextActionStoreState} from '../../shared/store/next-action-store.state';
import {NextActionStore} from '../../shared/store/next-action-store.actions';
import {TagStoreState} from "../../shared/store/tag-store.state";

@Component({
  selector: 'app-next-action-details',
  templateUrl: './next-action-details.component.html',
  styleUrls: ['./next-action-details.component.scss']
})
export class NextActionDetailsComponent implements OnInit, OnDestroy {

  @Select(NextActionStoreState.getSelected) selectedAction$: Observable<NextAction>;
  @Select(TagStoreState.getTags) tags$ : Observable<ActionTag[]>

  tagLoaded = false;
  availableTags: ActionTag[] = [];
  nextActionForm: UntypedFormGroup;

  private subs: Subscription[] = [];

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: UntypedFormBuilder,
    private store: Store
  ) {
    this.nextActionForm = this.fb.group({
      nextAction: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.subs.push(
      this.selectedAction$.pipe(
        tap(action => this.nextActionForm.patchValue({
          nextAction: action
        }))
      ).subscribe(),
      this.route.params.pipe(
        switchMap(params => this.store.dispatch(new NextActionStore.LoadAll()).pipe(map(()=> params))),
        switchMap(params => this.store.dispatch(new NextActionStore.Select(params.id)))
      ).subscribe(),
      this.tags$.pipe(
        tap(tags => this.availableTags = tags),
        tap(()=> this.tagLoaded = true)
      ).subscribe()
    )
  }

  onSubmit() {
    const nextAction: NextAction = this.nextActionForm.value.nextAction;
    if (nextAction.id) {
      this.subs.push(
        this.store.dispatch(new NextActionStore.UpdateAction(nextAction)).pipe(
          switchMap(()=> this.router.navigateByUrl('/nextActions')),
          tap(() => this.toastr.success('NextAction - Update')))
          .subscribe()
      );
    } else {
      this.toastr.error('NextAction - Update');
    }
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
