import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextActionCardComponent } from './next-action-card.component';

describe('NextActionCardComponent', () => {
  let component: NextActionCardComponent;
  let fixture: ComponentFixture<NextActionCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextActionCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextActionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
