import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActionType, NextAction } from '../../shared/next-action.model';
import { FormControl, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { ActionTag } from '../../shared/action-tag.model';

@Component({
  selector: 'gtd-next-action-card',
  templateUrl: './next-action-card.component.html',
  styleUrls: ['./next-action-card.component.scss']
})
export class NextActionCardComponent implements OnInit, OnChanges {

  @Input() nextAction: NextAction;
  @Input() tags: ActionTag[];

  protected readonly ActionType = ActionType;
  creationForm: UntypedFormGroup;
  showForm: boolean = false;
  isWaitingAction: boolean = false;

  @Output() editAction: EventEmitter<void> = new EventEmitter<void>();
  @Output('finishAction') finishActionEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() finishActionAndProject: EventEmitter<void> = new EventEmitter<void>();
  @Output() addNextAction: EventEmitter<NextAction> = new EventEmitter<NextAction>();
  @Output() switchStandard: EventEmitter<void> = new EventEmitter<void>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes?.nextAction?.currentValue && this.creationForm) {
      this.creationForm.patchValue({
        nextAction: {
          ...changes.nextAction.currentValue,
          actionPosition: changes.nextAction.currentValue.actionPosition + 1
        }
      })
    }
  }

  ngOnInit() {
    this.creationForm = new FormGroup<any>({
      nextAction: new FormControl('', Validators.required)
    });
  }

  closeAction() {
    this.finishActionEvent.emit();
  }

  closeActionAndProject() {
    this.finishActionAndProject.emit();
  }

  toggle() {
    this.showForm = !this.showForm;
  }

  edit() {
    this.editAction.emit()
  }

  submitForm() {
    const nextAction: NextAction = this.creationForm.value.nextAction;
    nextAction.project = this.nextAction.project;
    nextAction.actionType = this.isWaitingAction ? ActionType.WAITING : ActionType.STANDARD;
    nextAction.actionPosition = this.nextAction.actionPosition + 1;
    this.addNextAction.emit(nextAction);
  }

  switchToStandard() {
    this.switchStandard.emit();
  }
}
