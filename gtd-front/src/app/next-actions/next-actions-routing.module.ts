import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NextActionsListComponent } from './next-actions-list/next-actions-list.component';
import { NextActionDetailsComponent } from './next-action-details/next-action-details.component';

const routes: Routes = [
  { path: '', component: NextActionsListComponent },
  { path: ':id', component: NextActionDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NextActionsRoutingModule { }
