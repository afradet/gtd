import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { Project } from '../project.model';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { ActionType, NextAction } from '../../shared/next-action.model';
import { NextActionService } from '../../shared/next-action.service';
import { ToastrService } from 'ngx-toastr';
import { ActionTag } from '../../shared/action-tag.model';
import { MatDialog } from '@angular/material/dialog';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'gtd-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent implements OnInit {

  @Input() project: Project;
  @Input() tags: ActionTag[];

  @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();
  needAction: boolean = false;
  showActions: boolean = false;
  creationForm: UntypedFormGroup;
  isWaitingAction: boolean = false;

  constructor(
    private nextActionService: NextActionService,
    private toastr: ToastrService,
    private fb: UntypedFormBuilder,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.needAction = !this.project.actions.some(action => !action.completed);
    this.creationForm = this.fb.group({
        nextAction: new NextAction({
          actionPosition: this.project.actions.length,
          project: { ...this.project },
          actionTags: []
        })
      }
    )
  }

  confirmDelete(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef).afterClosed().subscribe(confirm => {
      if (confirm) {
        this.deleteEvent.emit()
      }
    })
  }

  toggleDetails() {
    this.showActions = !this.showActions;
  }

  submitForm() {
    const newAction: NextAction = this.creationForm.value.nextAction;
    newAction.actionType = this.isWaitingAction ? ActionType.WAITING : ActionType.STANDARD;
    this.nextActionService.save(newAction).pipe(
      tap(action => {
        this.toastr.success('NextAction - Create');
        this.project.actions.push(action);
        this.needAction = false;
        this.toggleDetails();
      })
    ).subscribe();
  }
}
