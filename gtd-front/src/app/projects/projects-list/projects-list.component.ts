import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../project.service';
import {Project} from '../project.model';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {ActionTag} from '../../shared/action-tag.model';
import {Select} from "@ngxs/store";
import {TagStoreState} from "../../shared/store/tag-store.state";

@Component({
  selector: 'gtd-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit {

  @Select(TagStoreState.getTags) tags$ : Observable<ActionTag[]>

  projects: Project[];
  tags: ActionTag[];

  constructor(
    private projectService: ProjectService
  ) {
  }

  ngOnInit() {
    this.tags$
      .pipe(
        tap(tags => this.tags = tags)
      )
      .subscribe();
    this.loadProjects().subscribe();
  }

  deleteProject(id: number) {
    this.projectService.delete(id).pipe(
      catchError(()=> {
        return of();
      }),
      switchMap(() => this.loadProjects())
    ).subscribe();
  }

  loadProjects(): Observable<Project[]> {
    return this.projectService.findAll()
      .pipe(
        catchError(()=> {
          return of([]);
        }),
        tap(results => this.projects = results)
      )
  }
}
