import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from './project.model';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private readonly BASE_URL: string = environment.baseUrl + "/rest/projects";

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<Project[]> {
    return this.httpClient.get<Project[]>(this.BASE_URL);
  }

  save(project: Project): Observable<Project> {
    return this.httpClient.post<Project>(this.BASE_URL, project);
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete(`${this.BASE_URL}/${id}`)
  }
}
