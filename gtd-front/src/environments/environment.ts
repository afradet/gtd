// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    baseUrl: "",
    amplify: {
        aws_cognito_region: 'eu-west-1',
        aws_user_pools_id: 'eu-west-1_JtrGvKq5k',
        aws_user_pools_web_client_id: '5bergajgg8oh6ej317fshrundd',
        aws_mandatory_sign_in: 'enable'
    }
    // cognitoUserPoolId: 'eu-west-1_Tljdpew20',
    // cognitoAppClientId: '3uj7ij099oj6878c8h7u1r633t'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
