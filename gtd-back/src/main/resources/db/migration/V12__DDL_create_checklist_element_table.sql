-- Create sequence and table for ChecklistElement

CREATE SEQUENCE checklist_element_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE checklist_element
(
    id bigint NOT NULL DEFAULT nextval('checklist_element_id_seq'::regclass),
    is_checked boolean NOT NULL,
    label character varying(255) NOT NULL,
    checklist_id bigint,
    CONSTRAINT checklist_element_pk_id PRIMARY KEY (id),
    CONSTRAINT checklist_element_fk_checklist_id FOREIGN KEY (checklist_id)
        REFERENCES checklist (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
