-- Create joint table users - roles

CREATE TABLE tj_users_roles
(
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    CONSTRAINT users_roles_pk_ids PRIMARY KEY (user_id, role_id),
    CONSTRAINT users_roles_fk_user_id FOREIGN KEY (user_id)
        REFERENCES user_gtd (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT users_roles_fk_role_id FOREIGN KEY (role_id)
        REFERENCES role_gtd (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
