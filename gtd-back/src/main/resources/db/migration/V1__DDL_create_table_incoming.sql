-- Create "Incoming" sequence and table

CREATE SEQUENCE incoming_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE incoming
(
    id bigint NOT NULL DEFAULT nextval('incoming_id_seq'::regclass),
    title character varying(255) NOT NULL,
    content character varying(255),
    creation_date timestamp without time zone,
    completion_date timestamp without time zone,
    completed boolean NOT NULL,
    CONSTRAINT incoming_pk_id PRIMARY KEY (id)
)
