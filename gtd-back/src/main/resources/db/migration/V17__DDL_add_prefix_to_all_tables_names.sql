-- Rename all simple table to prefix them with t_
-- Aimed at facilitating jooq transition and avoir too mnay namespace conflict

ALTER TABLE action_tag
    RENAME TO t_action_tag;

ALTER TABLE checklist
    RENAME TO t_checklist;

ALTER TABLE checklist_element
    RENAME TO t_checklist_element;

ALTER TABLE incoming
    RENAME TO t_incoming;

ALTER TABLE next_action
    RENAME TO t_next_action;

ALTER TABLE project
    RENAME TO t_project;

ALTER TABLE role_gtd
    RENAME TO t_role_gtd;

ALTER TABLE user_gtd
    RENAME TO t_user_gtd;
