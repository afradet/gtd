-- Create "Project" sequence, table and constraint

CREATE SEQUENCE project_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE project
(
    id bigint NOT NULL DEFAULT nextval('project_id_seq'::regclass),
    completed boolean NOT NULL,
    completion_date timestamp without time zone,
    creation_date timestamp without time zone,
    name character varying(255) NOT NULL,
    status character varying(255),
    CONSTRAINT project_pk_id PRIMARY KEY (id),
    CONSTRAINT project_uk_name UNIQUE (name)
)
