-- Create "User" sequence, table and constraint

CREATE SEQUENCE user_gtd_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE user_gtd
(
    id bigint NOT NULL DEFAULT nextval('user_gtd_id_seq'::regclass),
    email character varying(50) NOT NULL,
    password character varying(120) NOT NULL,
    username character varying(20) NOT NULL,
    CONSTRAINT user_gtd_pk_id PRIMARY KEY (id),
    CONSTRAINT user_gtd_uk_username UNIQUE (username),
    CONSTRAINT user_gtd_uk_email UNIQUE (email)
)
