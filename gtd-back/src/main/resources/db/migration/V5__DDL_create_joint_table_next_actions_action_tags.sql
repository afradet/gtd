--- Create join table NextAction - NextActionTag

CREATE TABLE tj_next_actions_action_tags
(
    next_action_id bigint NOT NULL,
    action_tag_id bigint NOT NULL,
    CONSTRAINT tj_next_actions_action_tags_pk_ids PRIMARY KEY (next_action_id, action_tag_id),
    CONSTRAINT next_actions_action_tags_fk_next_action_id FOREIGN KEY (next_action_id)
        REFERENCES next_action (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT next_actions_action_tags_fk_action_tags_id FOREIGN KEY (action_tag_id)
        REFERENCES action_tag (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
