-- Create "Role" sequence, table and constraint

CREATE SEQUENCE role_gtd_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE role_gtd
(
    id bigint NOT NULL DEFAULT nextval('role_gtd_id_seq'::regclass),
    name character varying(20) NOT NULL,
    CONSTRAINT role_gtd_pk_id PRIMARY KEY (id),
    CONSTRAINT role_gtd_uk_name UNIQUE (name)
)
