package fr.afradet.gtd.back.shared.collection;

public final class Pair<K, V> {

  private final K key;
  private final V value;

  private Pair(K key, V value) {
    this.key = key;
    this.value = value;
  }

  public static <S, T> Pair<S, T> of(S key, T value) {
    return new Pair<>(key, value);
  }

  public K getKey() {
    return this.key;
  }

  public V getValue() {
    return this.value;
  }

}
