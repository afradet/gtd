package fr.afradet.gtd.back.shared.exception.infrastructure.primary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

@RestControllerAdvice
public class ExceptionHandlerController {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class);
    private static final String UNHANDLED_ERROR = "An unexpected error happens";

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ApiError> accessDeniedExceptionHandler(AccessDeniedException ex) {
        log.info("Denied access");
        return new ResponseEntity<>(
                new ApiError().setErrorMessage(ex.getMessage()),
                HttpStatus.FORBIDDEN

        );
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ApiError> badCredentialsExceptionHandler(BadCredentialsException ex) {
        log.info("Wrong authentication");
        return new ResponseEntity<>(
                new ApiError().setErrorMessage("Invalid credentials"),
                HttpStatus.UNAUTHORIZED
        );
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> globalExceptionHandler(Exception ex) {
        log.error(UNHANDLED_ERROR, ex);
        return new ResponseEntity<>(
                new ApiError()
                        .setErrorMessage(UNHANDLED_ERROR),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(
                new ApiError()
                        .setErrorMessage("Validation error")
                        .setErrorDetails(ex.getBindingResult().getAllErrors()
                                .stream()
                                .collect(Collectors.toMap(
                                        err -> ((FieldError) err).getField(),
                                        DefaultMessageSourceResolvable::getDefaultMessage
                                ))
                        ),
                HttpStatus.BAD_REQUEST
        );
    }
}
