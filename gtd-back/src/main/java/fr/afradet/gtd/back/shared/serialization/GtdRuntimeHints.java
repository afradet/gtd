package fr.afradet.gtd.back.shared.serialization;

import fr.afradet.gtd.back.shared.serialization.GtdRuntimeHints.JacksonRuntimeHints;
import java.util.HashSet;
import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportRuntimeHints;

@Configuration
@ImportRuntimeHints(value = JacksonRuntimeHints.class)
public class GtdRuntimeHints {

  static class JacksonRuntimeHints implements RuntimeHintsRegistrar {

    @Override
    public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
      hints.serialization().registerType(HashSet.class);
    }
  }
}
