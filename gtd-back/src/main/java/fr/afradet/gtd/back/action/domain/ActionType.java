package fr.afradet.gtd.back.action.domain;

public enum ActionType {
    STANDARD,
    WAITING,
    MAYBE
}
