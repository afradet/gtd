package fr.afradet.gtd.back.action.infrastructure.primary;

import fr.afradet.gtd.back.action.aplication.ActionTagService;
import jakarta.validation.Valid;
import java.util.List;
import java.util.Objects;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("rest/actionTags")
@PreAuthorize("hasRole('GTD_USER') or hasRole('GTD_ADMIN')")
public class ActionTagController {

    private final ActionTagService tagService;
    private final ActionTagMapper mapper;

    public ActionTagController(ActionTagService tagService, ActionTagMapper mapper) {
        this.tagService = tagService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<ActionTagDTO> getAllTags() {
        return mapper.toDtos(tagService.getAllTags());
    }

    @PostMapping
    public ActionTagDTO createTag(@Valid @RequestBody ActionTagDTO actionTag) {
        return mapper.toDto(tagService.save(mapper.toPojo(actionTag)));
    }

    @PutMapping("/{id}")
    public ActionTagDTO updateTag(@PathVariable("id") Long id, @Valid @RequestBody ActionTagDTO tag) {
        if (!Objects.equals(id, tag.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return mapper.toDto(tagService.save(mapper.toPojo(tag)));
    }
}
