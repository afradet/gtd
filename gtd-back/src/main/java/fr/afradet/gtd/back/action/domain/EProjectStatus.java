package fr.afradet.gtd.back.action.domain;

public enum EProjectStatus {
    CREATED,
    TASK_TODO,
    TASK_NEEDED,
    COMPLETED
}
