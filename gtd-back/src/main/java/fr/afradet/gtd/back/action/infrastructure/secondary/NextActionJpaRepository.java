package fr.afradet.gtd.back.action.infrastructure.secondary;

import fr.afradet.gtd.back.action.domain.ActionType;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NextActionJpaRepository extends JpaRepository<NextActionEntity, Long> {

  List<NextActionEntity> findByCompletedEquals(boolean completed, Sort sort);

  List<NextActionEntity> findByProject_Id(Long projectId);

  List<NextActionEntity> findByCompletedAndActionTypeAndActionTags_IdIn(Boolean completed, ActionType actionType, List<Long> tags);

}
