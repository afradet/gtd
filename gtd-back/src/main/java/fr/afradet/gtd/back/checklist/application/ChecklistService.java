package fr.afradet.gtd.back.checklist.application;

import fr.afradet.gtd.back.checklist.domain.Checklist;
import fr.afradet.gtd.back.checklist.domain.ChecklistActionType;
import fr.afradet.gtd.back.checklist.domain.ChecklistLight;
import fr.afradet.gtd.back.checklist.domain.ChecklistRepository;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ChecklistService {

    private final ChecklistRepository repository;

  public ChecklistService(ChecklistRepository repository) {
    this.repository = repository;
  }

    public List<ChecklistLight> getAllChecklist() {
        return repository.findAll();
    }

    public Checklist getChecklist(Long id) {
        return repository.findById(id).orElseThrow();
    }

    public Checklist createChecklist(Checklist dto) {
        return repository.save(dto);
    }

    @Transactional
    public Checklist updateChecklist(Checklist checklist) {
        return repository.save(checklist);
    }

    public void deleteChecklist(Long id) {
        repository.deleteById(id);
    }

    public Checklist execute(Long id, ChecklistActionType action) {
      return repository.save(repository.findById(id).orElseThrow().executeAction(action));
    }
}
