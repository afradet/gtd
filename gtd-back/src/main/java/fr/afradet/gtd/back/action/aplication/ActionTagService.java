package fr.afradet.gtd.back.action.aplication;

import fr.afradet.gtd.back.action.domain.ActionTag;
import fr.afradet.gtd.back.action.domain.ActionTagRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ActionTagService {

    public final ActionTagRepository repository;

    public ActionTagService(ActionTagRepository repository) {
        this.repository = repository;
    }

    public List<ActionTag> getAllTags() {
        return repository.findAll();
    }

    public ActionTag save(ActionTag tag) {
        return repository.save(tag);
    }
}
