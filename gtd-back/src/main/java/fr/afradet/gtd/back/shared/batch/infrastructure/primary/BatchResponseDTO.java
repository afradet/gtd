package fr.afradet.gtd.back.shared.batch.infrastructure.primary;

import java.util.List;

public class BatchResponseDTO<T> {

    private List<BatchResultDTO<T>> results;

    public List<BatchResultDTO<T>> getResults() {
        return results;
    }

    public BatchResponseDTO<T> setResults(List<BatchResultDTO<T>> results) {
        this.results = results;
        return this;
    }
}
