package fr.afradet.gtd.back.action.infrastructure.primary;

import lombok.Data;

@Data
public class ProjectLightDTO {
  private Long id;
  private String name;
}
