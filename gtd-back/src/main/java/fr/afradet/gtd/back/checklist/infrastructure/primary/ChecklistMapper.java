package fr.afradet.gtd.back.checklist.infrastructure.primary;

import fr.afradet.gtd.back.checklist.domain.Checklist;
import fr.afradet.gtd.back.checklist.domain.ChecklistLight;
import fr.afradet.gtd.back.shared.mapping.GlobalMapperConfig;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(config = GlobalMapperConfig.class, uses = ChecklistElementMapper.class)
public interface ChecklistMapper {

  Checklist toPojo(ChecklistDTO dto);

  ChecklistDTO toDto(Checklist pojo);

  List<Checklist> toPojos(List<ChecklistDTO> dtos);

  List<ChecklistDTO> toDtos(List<Checklist> pojos);

  ChecklistLightDTO toLightDto(ChecklistLight pojo);

  List<ChecklistLightDTO> toLightDtos(List<ChecklistLight> pojos);
}
