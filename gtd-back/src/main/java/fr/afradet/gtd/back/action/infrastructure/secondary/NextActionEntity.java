package fr.afradet.gtd.back.action.infrastructure.secondary;

import fr.afradet.gtd.back.action.domain.ActionType;
import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.shared.coverage.Generated;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "t_next_action")
public class NextActionEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String label;
  private String content;
  private boolean completed;
  private LocalDateTime creationDate;
  private LocalDateTime completionDate;
  private LocalDateTime deadlineDate;
  private int actionPosition;
  @Enumerated(EnumType.STRING)
  private ActionType actionType;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "project_id")
  private ProjectEntity project;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "tj_next_actions_action_tags",
      joinColumns = {@JoinColumn(name = "next_action_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "action_tag_id", referencedColumnName = "id")})
  private Set<ActionTagEntity> actionTags;

  public static NextActionEntity fromDomain(NextAction action) {
   return new NextActionEntity()
       .setId(action.getId())
       .setLabel(action.getLabel())
       .setContent(action.getContent())
       .setCompleted(action.isCompleted())
       .setCreationDate(action.getCreationDate())
       .setCompletionDate(action.getCompletionDate())
       .setDeadlineDate(action.getDeadlineDate())
       .setActionPosition(action.getActionPosition())
       .setActionType(action.getActionType())
       .setProject(Optional.ofNullable(action.getProject()).map(ProjectEntity::fromDomain).orElse(null))
       .setActionTags(Optional.ofNullable(action.getActionTags())
           .map(tags -> tags.stream().map(ActionTagEntity::fromDomain).collect(Collectors.toSet()))
           .orElse(Collections.emptySet()));
  }

  public NextAction toDomain() {
    return new NextAction()
        .setId(id)
        .setLabel(label)
        .setContent(content)
        .setCompleted(completed)
        .setCreationDate(creationDate)
        .setCompletionDate(completionDate)
        .setDeadlineDate(deadlineDate)
        .setActionPosition(actionPosition)
        .setActionType(actionType)
        .setActionTags(actionTags.stream().map(ActionTagEntity::toDomain).collect(Collectors.toSet()));
  }

  public NextAction toDomainWithProject() {
    return this.toDomain().setProject(Optional.ofNullable(project).map(ProjectEntity::toDomain).orElse(null));
  }

  public NextActionEntity addTag(ActionTagEntity tag) {
    if (actionTags == null) {
      actionTags = new HashSet<>();
    }
    this.actionTags.add(tag);
    return this;
  }

  public NextActionEntity removeTag(ActionTagEntity tag) {
    this.actionTags.remove(tag);
    return this;
  }

  @Generated
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NextActionEntity that = (NextActionEntity) o;
    return completed == that.completed && actionPosition == that.actionPosition
        && Objects.equals(id, that.id) && Objects.equals(label, that.label)
        && Objects.equals(content, that.content) && Objects.equals(creationDate,
        that.creationDate) && Objects.equals(completionDate, that.completionDate)
        && Objects.equals(deadlineDate, that.deadlineDate) && actionType == that.actionType;
  }

  @Generated
  @Override
  public int hashCode() {
    return Objects.hash(id, label, content, completed, creationDate, completionDate, deadlineDate,
        actionPosition, actionType);
  }

  @Generated
  @Override
  public String toString() {
    return "NextActionEntity{" +
        "id=" + id +
        ", label='" + label + '\'' +
        ", content='" + content + '\'' +
        ", completed=" + completed +
        ", creationDate=" + creationDate +
        ", completionDate=" + completionDate +
        ", deadlineDate=" + deadlineDate +
        ", actionPosition=" + actionPosition +
        ", actionType=" + actionType +
        '}';
  }
}
