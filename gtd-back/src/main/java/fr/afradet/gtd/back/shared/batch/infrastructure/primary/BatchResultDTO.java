package fr.afradet.gtd.back.shared.batch.infrastructure.primary;

public class BatchResultDTO<T> {

    private T body;
    private Integer statusCode;

    public T getBody() {
        return body;
    }

    public BatchResultDTO<T> setBody(T body) {
        this.body = body;
        return this;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public BatchResultDTO<T> setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @Override
    public String toString() {
        return "BatchResultDTO{" +
            "body=" + body +
            ", statusCode=" + statusCode +
            '}';
    }
}
