package fr.afradet.gtd.back.checklist.domain;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import lombok.Data;

@Data
public class Checklist {

    private Long id;
    private String name;
    private LocalDate creationDate;
    private LocalDate lastResetDate;
    private ChronoUnit resetFrequencyUnit;
    private Integer resetFrequencyValue;
    private List<ChecklistElement> checklistElements;

    public Checklist executeAction(ChecklistActionType action) {
        switch (action) {
            case RESET -> checklistElements.forEach(e -> e.setChecked(false));
            default -> throw new IllegalArgumentException("Invalid action");
        }
        return this;
    }
}
