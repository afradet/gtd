package fr.afradet.gtd.back.action.domain;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository {

    List<Project> findAllByCompleted(boolean b);

    boolean existsById(Long id);

    Project save(Project project);

    Optional<Project> findById(Long id);
}
