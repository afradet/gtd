package fr.afradet.gtd.back.incoming.infrastructure.secondary;

import fr.afradet.gtd.back.incoming.domain.Incoming;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.LocalDateTime;
import lombok.Data;

@Data
@Entity
@Table(name = "t_incoming")
public class IncomingEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String title;
  private String content;
  @Column(name = "creation_date", columnDefinition = "TIMESTAMP")
  private LocalDateTime creationDate;
  @Column(name = "completion_date", columnDefinition = "TIMESTAMP")
  private LocalDateTime completionDate;
  private Boolean completed;

  static IncomingEntity fromDomain(Incoming incoming) {
    return new IncomingEntity()
        .setId(incoming.getId())
        .setTitle(incoming.getTitle())
        .setContent(incoming.getContent())
        .setCreationDate(incoming.getCreationDate())
        .setCompletionDate(incoming.getCompletionDate())
        .setCompleted(incoming.getCompleted());

  }

  Incoming toDomain() {
    return new Incoming()
        .setId(id)
        .setTitle(title)
        .setContent(content)
        .setCreationDate(creationDate)
        .setCompletionDate(completionDate)
        .setCompleted(completed);
  }

}
