package fr.afradet.gtd.back.action.infrastructure.secondary;

import fr.afradet.gtd.back.action.domain.Project;
import fr.afradet.gtd.back.action.domain.ProjectRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ProjectRepositoryImpl implements ProjectRepository {

    private final ProjectJpaRepository repository;

    public ProjectRepositoryImpl(ProjectJpaRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Project> findAllByCompleted(boolean b) {
        return repository.findAllByCompleted(b).stream().map(ProjectEntity::toDomainWithAction).toList();
    }

    @Override
    public boolean existsById(Long id) {
        return repository.existsById(id);
    }

    @Override
    public Project save(Project project) {
        return repository.save(ProjectEntity.fromDomain(project)).toDomain();
    }

    @Override
    public Optional<Project> findById(Long id) {
        return repository.findById(id).map(ProjectEntity::toDomainWithAction);
    }
}
