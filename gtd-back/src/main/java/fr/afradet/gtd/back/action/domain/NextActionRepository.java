package fr.afradet.gtd.back.action.domain;

import java.util.List;
import java.util.Optional;

public interface NextActionRepository {

    List<NextAction> findByCompleted(boolean completed);

    List<NextAction> findByCompletedAndTypeAndTagsId(Boolean completed, ActionType actionType, List<Long> tagId);

    Optional<NextAction> findById(Long id);

    void saveAll(List<NextAction> list);

    List<NextAction> findByProjectId(Long projectId);

    NextAction save(NextAction action);
}
