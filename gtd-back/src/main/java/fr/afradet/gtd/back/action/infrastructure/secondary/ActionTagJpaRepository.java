package fr.afradet.gtd.back.action.infrastructure.secondary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionTagJpaRepository extends JpaRepository<ActionTagEntity, Long> {

}
