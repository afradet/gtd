package fr.afradet.gtd.back.checklist.infrastructure.primary;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ChecklistElementDTO {

    private Long id;
    @NotBlank
    private String label;
    @NotNull
    private Boolean checked;
}
