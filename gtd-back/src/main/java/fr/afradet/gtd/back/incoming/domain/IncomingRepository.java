package fr.afradet.gtd.back.incoming.domain;

import java.util.List;
import java.util.Optional;

public interface IncomingRepository {

    List<Incoming> findAllByCompleted(boolean completed);

    Optional<Incoming> findById(Long id);

    Incoming save(Incoming incoming);

    List<Incoming> saveAll(List<Incoming> list);
}
