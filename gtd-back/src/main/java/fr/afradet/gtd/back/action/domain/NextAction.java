package fr.afradet.gtd.back.action.domain;

import java.time.LocalDateTime;
import java.util.Set;
import lombok.Data;

@Data
public class NextAction {

  private Long id;
  private String label;
  private String content;
  private boolean completed;
  private LocalDateTime creationDate;
  private LocalDateTime completionDate;
  private LocalDateTime deadlineDate;
  private int actionPosition;
  private ActionType actionType;
  private Project project;
  private Set<ActionTag> actionTags;

  public NextAction complete(LocalDateTime timestamp) {
    completed = true;
    completionDate = timestamp;
    return this;
  }
}
