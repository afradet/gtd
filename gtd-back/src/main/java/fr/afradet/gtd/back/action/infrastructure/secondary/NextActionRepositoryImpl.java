package fr.afradet.gtd.back.action.infrastructure.secondary;

import fr.afradet.gtd.back.action.domain.ActionType;
import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.NextActionRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class NextActionRepositoryImpl implements NextActionRepository {

  private final NextActionJpaRepository repository;

  public NextActionRepositoryImpl(NextActionJpaRepository repository) {
    this.repository = repository;
  }

  @Override
  public List<NextAction> findByCompleted(boolean completed) {
    return repository.findByCompletedEquals(completed, Sort.by(Direction.ASC, "deadlineDate", "id"))
        .stream()
        .map(NextActionEntity::toDomainWithProject)
        .toList();
  }

  @Override
  public List<NextAction> findByCompletedAndTypeAndTagsId(Boolean completed, ActionType actionType,
      List<Long> tagId) {
    return repository.findByCompletedAndActionTypeAndActionTags_IdIn(completed, actionType, tagId)
        .stream()
        .map(NextActionEntity::toDomainWithProject)
        .toList();
  }

  @Override
  public NextAction save(NextAction nextAction) {
    return repository.save(NextActionEntity.fromDomain(nextAction)).toDomainWithProject();
  }

  @Override
  public Optional<NextAction> findById(Long id) {
    return repository.findById(id).map(NextActionEntity::toDomainWithProject);
  }

  @Override
  public void saveAll(List<NextAction> actions) {
    repository.saveAll(actions.stream().map(NextActionEntity::fromDomain).toList());
  }

  @Override
  public List<NextAction> findByProjectId(Long projectId) {
    return repository.findByProject_Id(projectId).stream()
        .map(NextActionEntity::toDomainWithProject).toList();
  }
}
