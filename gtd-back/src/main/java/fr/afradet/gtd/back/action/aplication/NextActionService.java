package fr.afradet.gtd.back.action.aplication;

import fr.afradet.gtd.back.action.domain.EProjectStatus;
import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.NextActionRepository;
import fr.afradet.gtd.back.action.domain.Project;
import fr.afradet.gtd.back.action.domain.ProjectRepository;
import fr.afradet.gtd.back.shared.exception.domain.ResourceNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
public class NextActionService {

    private final NextActionRepository nextActionRepository;
    private final ProjectRepository projectRepository;

    public NextActionService(
        NextActionRepository nextActionRepository,
        ProjectRepository projectRepository
    ) {
        this.nextActionRepository = nextActionRepository;
        this.projectRepository = projectRepository;
    }

    public List<NextAction> findAll() {
        return nextActionRepository.findByCompleted(false);
    }

    @Transactional
    public NextAction create(NextAction newAction) {
        if (newAction.getProject() != null) {
            Project project;
            if (newAction.getProject().getId() != null) {
                project = projectRepository.findById(newAction.getProject().getId()).orElseThrow();
            } else {
                project = projectRepository.save(
                    newAction.getProject()
                        .setCompletionDate(LocalDateTime.now())
                        .setStatus(EProjectStatus.TASK_TODO)
                );
            }
            newAction.setProject(project);
        }
        return nextActionRepository.save(newAction);
    }

    public void delete(Long id) {
        nextActionRepository.save(
            nextActionRepository.findById(id).orElseThrow().complete(LocalDateTime.now())
        );
    }

    public NextAction getById(Long id) {
        return nextActionRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public NextAction update(NextAction updatedAction, Long id) {
        if (id == null || !id.equals(updatedAction.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        var action = nextActionRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        action.setLabel(updatedAction.getLabel());
        action.setContent(updatedAction.getContent());
        action.setDeadlineDate(updatedAction.getDeadlineDate());
        return nextActionRepository.save(updatedAction);
    }
}
