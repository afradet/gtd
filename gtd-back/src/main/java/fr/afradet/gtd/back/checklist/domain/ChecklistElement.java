package fr.afradet.gtd.back.checklist.domain;

import lombok.Data;

@Data
public class ChecklistElement {

    private Long id;
    private String label;
    private boolean checked;
}
