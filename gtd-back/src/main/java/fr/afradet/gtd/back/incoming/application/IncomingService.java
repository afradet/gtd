package fr.afradet.gtd.back.incoming.application;

import fr.afradet.gtd.back.incoming.domain.Incoming;
import fr.afradet.gtd.back.incoming.domain.IncomingRepository;
import fr.afradet.gtd.back.shared.batch.domain.BatchOperation;
import fr.afradet.gtd.back.shared.batch.domain.BatchResult;
import fr.afradet.gtd.back.shared.batch.domain.HttpStatus;
import fr.afradet.gtd.back.shared.batch.domain.OperationType;
import fr.afradet.gtd.back.shared.exception.domain.ResourceNotFoundException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IncomingService {

    private final IncomingRepository incomingRepository;

    public IncomingService(IncomingRepository incomingRepository) {
        this.incomingRepository = incomingRepository;
    }

    public List<Incoming> getAllIncomings() {
        return incomingRepository.findAllByCompleted(false);
    }

    public Incoming getIncoming(Long id) {
        return incomingRepository.findById(id).orElse(new Incoming());
    }

    @Transactional
    public void delete(Long id) {
        incomingRepository.save(
            incomingRepository.findById(id).orElseThrow(ResourceNotFoundException::new).complete()
        );
    }

    public Incoming update(Long id, Incoming incoming) {
        if (id == null || !id.equals(incoming.getId())) {
            throw new ResourceNotFoundException();
        }
        return incomingRepository.save(
            incomingRepository.findById(id).orElseThrow(ResourceNotFoundException::new)
                .setTitle(incoming.getTitle())
                .setContent(incoming.getContent())
        );
    }

    public Incoming create(Incoming incoming) {
        return incomingRepository.save(incoming);
    }

    public List<BatchResult<Incoming>> batch(List<BatchOperation<Incoming>> operations) {
        LocalDateTime now = LocalDateTime.now();

        Map<Boolean, List<BatchOperation<Incoming>>> sortedOp = IntStream.range(0, operations.size())
                .mapToObj(i -> operations.get(i).setOrder(i))
                .collect(Collectors.groupingBy(operation -> OperationType.CREATE.equals(operation.getOperation())));

        List<BatchOperation<Incoming>> createOp = sortedOp.getOrDefault(true, Collections.emptyList());
        List<Incoming> createResults = incomingRepository.saveAll(
                createOp.stream().map(BatchOperation::getBody).map(incoming -> incoming.setCreationDate(now)).toList());
        List<BatchResult<Incoming>> createOrderedResults = assembleResults(createOp, createResults);

        List<BatchOperation<Incoming>> updateOp = sortedOp.getOrDefault(false, Collections.emptyList());
        List<Incoming> updateResults = incomingRepository.saveAll(
                updateOp.stream().map(BatchOperation::getBody).toList());
        List<BatchResult<Incoming>> updateOrderedResults = assembleResults(updateOp, updateResults);

        return Stream.concat(createOrderedResults.stream(), updateOrderedResults.stream())
                .sorted(Comparator.comparing(BatchResult::getOrder))
                .toList();
    }

    private List<BatchResult<Incoming>> assembleResults(List<BatchOperation<Incoming>> operations, List<Incoming> results) {
        if (operations.size() != results.size()) {
            throw new RuntimeException("KO");
        }
        return IntStream.range(0, operations.size())
                .mapToObj(i -> new BatchResult<Incoming>()
                        .setBody(results.get(i))
                        .setHttpStatus(HttpStatus.OK)
                        .setOrder(operations.get(i).getOrder())
                )
                .toList();
    }
}
