package fr.afradet.gtd.back.incoming.infrastructure.primary;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class IncomingDTO {

    private Long id;
    @NotNull
    private String title;
    private String content;
    private LocalDateTime creationDate;
    private LocalDateTime completionDate;
    private Boolean completed;
}
