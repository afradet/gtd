package fr.afradet.gtd.back.checklist.infrastructure.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Data
public class ChecklistDTO {

    private Long id;

    @NotBlank
    private String name;

    private LocalDate creationDate;

    private LocalDate lastResetDate;

    private ChronoUnit resetFrequencyUnit;

    private Integer resetFrequencyValue;

    private List<ChecklistElementDTO> checklistElements = new ArrayList<>();
}
