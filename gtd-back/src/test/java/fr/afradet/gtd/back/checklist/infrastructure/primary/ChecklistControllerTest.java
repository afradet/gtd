package fr.afradet.gtd.back.checklist.infrastructure.primary;

import static fr.afradet.gtd.back.checklist.fixture.ChecklistFixture.checklistEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.afradet.gtd.back.checklist.domain.Checklist;
import fr.afradet.gtd.back.checklist.domain.ChecklistElement;
import fr.afradet.gtd.back.shared.ControllerTest;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.http.MediaType;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

// TODO solve issue with data ids
@Testcontainers
class ChecklistControllerTest extends ControllerTest {

  @Container
  @ServiceConnection
  static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

  @Test
  void getAllChecklistsSecurityCheck() throws Exception {
    mvc.perform(get("/rest/checklists")
            .contentType((MediaType.APPLICATION_JSON)))
        .andExpect(status().isUnauthorized());
  }

  @Test
  void getAllChecklists() throws Exception {
    // GIVEN
    var expected = checklistEntity();
    var entities = List.of(
        expected,
        checklistEntity(),
        checklistEntity()
    );
    checklistRepository.saveAll(entities);

    // WHEN
    var resultString = mvc.perform(get("/rest/checklists")
            .with(userGtd())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andReturn()
        .getResponse()
        .getContentAsString();

    List<ChecklistLightDTO> result = mapper.readValue(resultString, new TypeReference<>() {
    });
    assertThat(result)
        .hasSize(3)
        .anySatisfy(c -> {
          assertThat(c.id()).isEqualTo(expected.getId());
          assertThat(c.name()).isEqualTo(expected.getName());
        });
  }

  @Test
  void getChecklist() throws Exception {
    var ck = checklistRepository.save(checklistEntity());
    String resultString = mvc.perform(get("/rest/checklists/%s".formatted(ck.getId()))
            .with(userGtd())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andReturn()
        .getResponse()
        .getContentAsString();

    ChecklistDTO result = mapper.readValue(resultString, new TypeReference<>() {
    });

    assertThat(result).satisfies(c -> {
      assertThat(c.getId()).isEqualTo(ck.getId());
      assertThat(c.getName()).isEqualTo(ck.getName());
      assertThat(c.getName()).isEqualTo(ck.getName());
      assertThat(c.getChecklistElements()).hasSize(ck.getChecklistElements().size());
    });
  }

  @Test
  void createChecklist() throws Exception {
    String resultString = mvc.perform(post("/rest/checklists")
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(new Checklist().setName("ChecklistName")))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name", is("ChecklistName")))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andReturn()
        .getResponse()
        .getContentAsString();
    ChecklistDTO resultDto = mapper.readValue(resultString, ChecklistDTO.class);

    assertThat(checklistRepository.findAll()).singleElement().satisfies(c -> {
      assertThat(c.getId()).isEqualTo(resultDto.getId());
      assertThat(c.getName()).isEqualTo("ChecklistName");
    });
  }

  @Test
  void updateChecklist() throws Exception {
    var ck = checklistRepository.save(checklistEntity());

    var updated = new Checklist()
        .setName("ChecklistName")
        .setId(ck.getId())
        .setChecklistElements(List.of(
            new ChecklistElement().setId(ck.getChecklistElements().get(0).getId()).setChecked(false)
                .setLabel("Element"),
            new ChecklistElement().setChecked(true).setLabel("Element2"),
            new ChecklistElement().setChecked(false).setLabel("Element3")
        ));

    mvc.perform(put("/rest/checklists/%s".formatted(ck.getId()))
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(updated))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
//            .andExpect(jsonPath("$.id", is(ck.getId().toString())))
        .andExpect(jsonPath("$.name", is("ChecklistName")));

    assertThat(checklistRepository.findAll()).singleElement().satisfies(c -> {
      assertThat(c.getName()).isEqualTo("ChecklistName");
      assertThat(c.getChecklistElements()).hasSize(3)
          .anySatisfy(cke -> {
            assertThat(cke.getLabel()).isEqualTo("Element");
            assertThat(cke.isChecked()).isFalse();
          })
          .anySatisfy(cke -> {
            assertThat(cke.getLabel()).isEqualTo("Element2");
            assertThat(cke.isChecked()).isTrue();
          })
          .anySatisfy(cke -> {
            assertThat(cke.getLabel()).isEqualTo("Element3");
            assertThat(cke.isChecked()).isFalse();
          });
    });
  }

  @Test
  void deleteChecklist() throws Exception {
    mvc.perform(delete("/rest/checklists/-1")
            .with(userGtd())
            .with(csrf()))
        .andExpect(status().isOk());

    assertThat(checklistRepository.findAll()).isEmpty();
  }
}
