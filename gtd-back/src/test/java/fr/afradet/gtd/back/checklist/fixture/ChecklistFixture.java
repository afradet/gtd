package fr.afradet.gtd.back.checklist.fixture;

import fr.afradet.gtd.back.checklist.infrastructure.secondary.ChecklistElementEntity;
import fr.afradet.gtd.back.checklist.infrastructure.secondary.ChecklistEntity;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

public class ChecklistFixture {

  public static ChecklistEntity checklistEntity() {
    var id = new Random().nextLong();
    var idElem = new Random().nextLong();
    return new ChecklistEntity()
        .setName("My checklist %s".formatted(id))
        .setCreationDate(LocalDate.now())
        .setChecklistElements(
            List.of(
                new ChecklistElementEntity().setLabel("Checklist element %s".formatted(idElem))
            )
        );
  }

}
