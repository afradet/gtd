package fr.afradet.gtd.back.shared;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.afradet.gtd.back.action.infrastructure.secondary.ActionTagJpaRepository;
import fr.afradet.gtd.back.action.infrastructure.secondary.NextActionJpaRepository;
import fr.afradet.gtd.back.action.infrastructure.secondary.ProjectJpaRepository;
import fr.afradet.gtd.back.checklist.infrastructure.secondary.ChecklistJpaRepository;
import fr.afradet.gtd.back.incoming.infrastructure.secondary.IncomingJpaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public abstract class ControllerTest {

    @Autowired
    protected MockMvc mvc;
    @Autowired
    protected ObjectMapper mapper;
    @Autowired
    protected NextActionJpaRepository nextActionRepository;
    @Autowired
    protected ProjectJpaRepository projectRepository;
    @Autowired
    protected ChecklistJpaRepository checklistRepository;
    @Autowired
    protected IncomingJpaRepository incomingRepository;
    @Autowired
    protected ActionTagJpaRepository actionTagRepository;

    @BeforeEach
    @Transactional
    protected void setUp() {
        nextActionRepository.deleteAll();
        projectRepository.deleteAll();
        actionTagRepository.deleteAll();
        incomingRepository.deleteAll();
        checklistRepository.deleteAll();
    }

    protected String asJsonString(final Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor userGtd() {
        return user("user").roles("GTD_USER");
    }

    protected SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor adminGtd() {
        return user("user").roles("GTD_ADMIN");
    }
}
