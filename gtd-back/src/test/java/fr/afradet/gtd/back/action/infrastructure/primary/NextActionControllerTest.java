package fr.afradet.gtd.back.action.infrastructure.primary;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.afradet.gtd.back.action.domain.ActionType;
import fr.afradet.gtd.back.action.domain.EProjectStatus;
import fr.afradet.gtd.back.action.infrastructure.secondary.ActionTagEntity;
import fr.afradet.gtd.back.action.infrastructure.secondary.NextActionEntity;
import fr.afradet.gtd.back.action.infrastructure.secondary.ProjectEntity;
import fr.afradet.gtd.back.shared.ControllerTest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.http.MediaType;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
class NextActionControllerTest extends ControllerTest {

  @Container
  @ServiceConnection
  static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

  private List<ProjectEntity> projects;
  private List<ActionTagEntity> tags;
  private List<NextActionEntity> nextActions;

  @BeforeEach
  public void setUp() {
    super.setUp();

    projects = projectRepository.saveAll(List.of(
        new ProjectEntity().setName("MyProject1").setStatus(EProjectStatus.TASK_TODO)
            .setCompleted(false),
        new ProjectEntity().setName("MyProject2").setCompleted(false),
        new ProjectEntity().setName("MyProject2").setCompleted(true)
    ));

    tags = actionTagRepository.saveAll(List.of(
        new ActionTagEntity().setLabel("TAG1").setIcon("delete").setColor("#12345"),
        new ActionTagEntity().setLabel("TAG2").setIcon("delete").setColor("#12345")
    ));

    nextActions = nextActionRepository.saveAll(List.of(
        new NextActionEntity()
            .setId(1L)
            .setProject(projects.get(0))
            .setLabel("NextActionLabel1")
            .setContent("NextActionContent1")
            .setActionType(ActionType.WAITING)
            .setActionPosition(1)
            .setCompleted(false)
            .setActionTags(Set.of(tags.get(0), tags.get(1))),
        new NextActionEntity()
            .setId(2L)
            .setLabel("NextActionLabel2")
            .setContent("NextActionContent2")
            .setActionType(ActionType.STANDARD)
            .setCompleted(false)
            .setActionTags(Set.of(tags.get(1))),
        new NextActionEntity()
            .setId(3L)
            .setProject(projects.get(1))
            .setLabel("NextActionLabel3")
            .setContent("NextActionContent3")
            .setActionType(ActionType.STANDARD)
            .setActionPosition(1)
            .setCompleted(false)
            .setActionTags(Set.of(tags.get(0), tags.get(1)))
            .setDeadlineDate(LocalDateTime.now()),
        new NextActionEntity()
            .setId(4L)
            .setLabel("NextActionLabel4")
            .setContent("NextActionContent4")
            .setActionType(ActionType.WAITING)
            .setCompleted(false),
        new NextActionEntity()
            .setId(5L)
            .setLabel("NextActionLabel5")
            .setContent("NextActionContent5")
            .setActionType(ActionType.MAYBE)
            .setCompleted(false)
            .setDeadlineDate(LocalDateTime.now().plusDays(3)),
        new NextActionEntity()
            .setId(6L)
            .setLabel("NextActionLabel6")
            .setContent("NextActionContent6")
            .setActionType(ActionType.MAYBE)
            .setCompleted(true)
    ));
  }

  @Test
  void getAllChecklistsSecurityCheck() throws Exception {
    mvc.perform(get("/rest/nextActions")
            .contentType((MediaType.APPLICATION_JSON)))
        .andExpect(status().isUnauthorized());
  }

  @Test
  void getNextActions_noFilter() throws Exception {
    String resultString = mvc.perform(get("/rest/nextActions")
            .with(userGtd())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(5)))
        .andReturn()
        .getResponse()
        .getContentAsString();

    List<NextActionDTO> result = mapper.readValue(resultString, new TypeReference<>() {
    });
    assertThat(result).hasSize(5)
        .extracting("id")
        .containsExactly(
            nextActions.get(2).getId(),
            nextActions.get(4).getId(),
            nextActions.get(0).getId(),
            nextActions.get(1).getId(),
            nextActions.get(3).getId()
        );
  }

  // TODO replace ?
  @Test
  void getNextActions_actionFilter() throws Exception {
    mvc.perform(get("/rest/nextActions")
            .with(userGtd())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(5)));
  }

  // TODO create with project, test positions, etc
  @Test
  void createNextAction() throws Exception {
    String resultString = mvc.perform(post("/rest/nextActions")
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(
                new NextActionDTO()
                    .setLabel("nextActionsLabel")
                    .setActionType(ActionType.WAITING.name())
                    .setActionPosition(0)
                    .setCompleted(false)
                    .setActionTags(tags.stream().map(this::map).collect(Collectors.toSet()))))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.label", is("nextActionsLabel")))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andExpect(jsonPath("$.actionType", is(ActionType.WAITING.name())))
        .andExpect(jsonPath("$.actionTags", hasSize(2)))
        .andReturn()
        .getResponse()
        .getContentAsString();

    NextActionDTO resultDto = mapper.readValue(resultString, NextActionDTO.class);

    assertThat(nextActionRepository.findById(resultDto.getId())).hasValueSatisfying(a -> {
      assertThat(a.getActionTags()).hasSize(2);
    });
  }

  @Test
  void createNextActionWithProject() throws Exception {
    String resultString = mvc.perform(post("/rest/nextActions")
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(
                new NextActionDTO()
                    .setLabel("nextActionsLabel")
                    .setActionType(ActionType.WAITING.name())
                    .setActionPosition(0)
                    .setCompleted(false)
                    .setActionTags(tags.stream().map(this::map).collect(Collectors.toSet()))
                    .setProject(new ProjectLightDTO().setName("My project"))
            ))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.label", is("nextActionsLabel")))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andExpect(jsonPath("$.actionType", is(ActionType.WAITING.name())))
        .andExpect(jsonPath("$.actionTags", hasSize(2)))
        .andReturn()
        .getResponse()
        .getContentAsString();

    NextActionDTO resultDto = mapper.readValue(resultString, NextActionDTO.class);

    assertThat(nextActionRepository.findById(resultDto.getId())).hasValueSatisfying(a -> {
      assertThat(a.getActionTags()).hasSize(2);
    });
    assertThat(projectRepository.findById(resultDto.getProject().getId())).hasValueSatisfying(
        p -> assertThat(p.getName()).isEqualTo("My project")
    );
  }

  @Test
  void updateNextAction() throws Exception {
    var id = nextActions.get(1).getId();

    String resultString = mvc.perform(put("/rest/nextActions/%s".formatted(id))
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(
                new NextActionDTO()
                    .setId(id)
                    .setLabel("new label")
                    .setContent("adding content is cool")
                    .setActionType(ActionType.STANDARD.name())
                    .setActionPosition(0)
                    .setCompleted(false)
                    .setActionTags(Set.of(map(tags.get(0))))))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.label", is("new label")))
        .andExpect(jsonPath("$.content", is("adding content is cool")))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andExpect(jsonPath("$.actionType", is(ActionType.STANDARD.name())))
        .andExpect(jsonPath("$.actionTags", hasSize(1)))
        .andReturn()
        .getResponse()
        .getContentAsString();

    NextActionDTO resultDto = mapper.readValue(resultString, NextActionDTO.class);

    assertThat(nextActionRepository.findById(resultDto.getId())).hasValueSatisfying(a -> {
      assertThat(a.getActionTags()).singleElement().isEqualTo(tags.get(0));
    });
  }

  @Test
  void getNextAction() throws Exception {
    var action = nextActions.get(0);
    mvc.perform(get("/rest/nextActions/%s".formatted(action.getId()))
            .with(userGtd())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(action.getId().intValue())))
        .andExpect(jsonPath("$.label", is(action.getLabel())))
        .andExpect(jsonPath("$.content", is(action.getContent())))
        .andExpect(jsonPath("$.actionType", is(action.getActionType().name())))
        .andExpect(jsonPath("$.actionPosition", is(action.getActionPosition())))
        .andExpect(jsonPath("$.project.id", is(action.getProject().getId().intValue())))
        .andExpect(jsonPath("$.project.name", is(action.getProject().getName())));
  }

  @Test
  void deleteNextAction() throws Exception {
    var id = nextActions.get(0).getId();
    mvc.perform(delete("/rest/nextActions/%s".formatted(id))
        .with(userGtd())
        .with(csrf())
    );

    assertThat(nextActionRepository.findById(id)).hasValueSatisfying(a -> {
      assertThat(a.isCompleted()).isTrue();
      assertThat(a.getCompletionDate()).isNotNull();
    });
  }

  private ActionTagDTO map(ActionTagEntity entity) {
    return new ActionTagDTO()
        .setId(entity.getId())
        .setLabel(entity.getLabel())
        .setColor(entity.getColor())
        .setIcon(entity.getIcon());
  }
}
